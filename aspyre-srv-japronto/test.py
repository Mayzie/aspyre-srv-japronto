#!/usr/bin/env python

import sys

import __init__

class app():
    async def __call__(self, *args, **kwargs):
        return [('Content-Type', 'text/plain'), ('Server', 'Aspyre')], b'Hello world!'

a = __init__.JaprontoServer(application=app())
a(host='127.0.0.1', port=int(sys.argv[1]) if len(sys.argv) > 1 else 8000)
#cProfile.run("a(host='unix:///var/run/aspyre.sock')")
