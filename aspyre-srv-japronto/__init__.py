from asyncio import get_event_loop

from japronto import Application
from japronto.router import Router, Route
from japronto.router.matcher import Matcher
from japronto.router.route import compile

from functools import partial

import inspect
import random
from struct import Struct

from uuid import uuid4


__all__ = ['JaprontoServer']


class JaprontoMatcher:
    __route = Route(None, None, None)

    def handle(self, request):
        return request.Response()

    def match_request(self, request):
        return JaprontoMatcher.__route, self.handle


class JaprontoRouter:
    def __init__(self, matcher_factory=None):
        self.__matcher = JaprontoMatcher()

    def get_matcher(self):
        return self.__matcher


class JaprontoServer:
    name = 'japronto'
    version = {
        'major': 0,
        'minor': 1,
        'patch': 0,
    }

    def __init__(self, application=None, loop=None):
        if not application:
            raise ValueError('No AspyreApplication given to JaprontoServer constructor.')

        self.__application__ = application
        self.__loop__ = None

        self.set_loop(loop)

    def __call__(self, host=None, port=None):
        self.__japronto_app__ = Application()
        #self.__japronto_app__._loop = self.__loop__  # Override the Japronto event loop.

        # Force override Japronto's routing functionality, so we can use Aspyre's.
        self.__japronto_router__ = JaprontoRouter()

        self.__japronto_app__._router = self.__japronto_router__
        self.__japronto_app__.run(host=host, port=port, debug=True)


    def set_loop(self, loop=None):
        if self.__loop__ and self.__loop__.is_running():
            self.__loop__.close()

        if not loop:
            self.__loop__ = get_event_loop()
        else:
            self.__loop__ = loop
