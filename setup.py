from setuptools import setup, find_packages

setup(
    name='aspyre-srv-japronto',
    version='0.1.0',
    packages=find_packages(),

    install_requires=[
        'japronto',
    ],

    author='Aspyre Framework',
    author_email='server+japronto@aspy.re',
    description='Interface for connecting Aspyre to the fast, C-based Japronto web server.',
    license='CC0',
    keywords='aspyre japronto server',
    url='aspy.re',

    namespace_packages=['aspyre.server']
)
